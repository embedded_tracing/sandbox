mod something;
mod sth_else;

pub use something::do_something;
pub use sth_else::do_something_else;

#[cfg(test)]
mod tests {

    use types::Timestamp;

    #[no_mangle]
    fn get() -> usize {
        1
    }

    #[no_mangle]
    fn get_physical() -> usize {
        2
    }

    #[no_mangle]
    fn get_current_time() -> Timestamp {
        Timestamp::from(44)
    }
}
