use client_library::Span;

pub fn do_something() {
    Span::new();
    println!("Does something ...");
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[test]
    fn does_it_work() {
        do_something();
        assert!(true);
    }
}
