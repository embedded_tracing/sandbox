use types::Timestamp;

pub struct Span;

extern "Rust" {
    fn get() -> usize;
    fn get_physical() -> usize;
    fn get_current_time() -> Timestamp;
}

impl Span {
    pub fn new() -> Self {
        println!("Current time: {}", unsafe { get_current_time().as_u64() });
        println!("Num CPUs: {}", unsafe { get() });
        Span
    }
}

impl Drop for Span {
    fn drop(&mut self) {
        println!("Num physical CPUs: {}", unsafe { get_physical() });
        println!("Current time: {}", unsafe { get_current_time().as_u64() });
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[no_mangle]
    fn get() -> usize {
        1
    }

    #[no_mangle]
    fn get_physical() -> usize {
        2
    }

    #[no_mangle]
    fn get_current_time() -> Timestamp {
        Timestamp::from(44)
    }

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[test]
    fn test_span() {
        let _span = Span::new();
        assert!(true);
    }
}
