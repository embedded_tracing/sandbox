use library::do_something;

#[no_mangle]
pub fn get_current_time() -> types::Timestamp {
    time::get_current_time()
}

// This is necessary since num_cpus does not use #[no_mangle]
#[no_mangle]
fn get() -> usize {
    num_cpus::get()
}

// This is necessary since num_cpus does not use #[no_mangle]
#[no_mangle]
fn get_physical() -> usize {
    num_cpus::get_physical()
}

fn main() {
    println!("Application start");

    do_something();

    println!("Application end");
}
