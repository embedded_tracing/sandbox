#[no_mangle]
pub fn get_current_time() -> types::Timestamp {
    time::get_current_time()
}
