use std::time::{SystemTime, UNIX_EPOCH};
use types::Timestamp;

pub fn get_current_time() -> Timestamp {
    let current_time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_micros() as u64;
    Timestamp::from(current_time)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
