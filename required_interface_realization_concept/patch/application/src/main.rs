use library::do_something;

fn main() {
    println!("Application start");

    do_something();

    println!("Application end");
}
