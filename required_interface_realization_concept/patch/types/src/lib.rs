pub struct Timestamp {
    time_data: [u8; 5],
}

impl Timestamp {
    pub fn from(counter: u64) -> Self {
        let mut time_data = [0; 5];
        let bytes = counter.to_be_bytes();
        let lsb = &bytes[3..8];
        for i in 0..5 {
            time_data[i] = lsb[i];
        }
        Timestamp { time_data }
    }

    pub fn get_data(self) -> [u8; 5] {
        self.time_data
    }

    pub fn as_u64(&self) -> u64 {
        let mut value: u64 = self.time_data[0].into();
        value <<= 8;
        value |= self.time_data[1] as u64;
        value <<= 8;
        value |= self.time_data[2] as u64;
        value <<= 8;
        value |= self.time_data[3] as u64;
        value <<= 8;
        value |= self.time_data[4] as u64;

        value
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
