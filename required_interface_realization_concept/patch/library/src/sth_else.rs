use crate::something;

pub fn do_something_else() {
    something::do_something();
    println!("Does something else");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_something_else() {
        do_something_else();
        assert!(true);
    }
}
