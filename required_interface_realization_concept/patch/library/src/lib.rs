mod something;
mod sth_else;

pub use something::do_something;
pub use sth_else::do_something_else;
