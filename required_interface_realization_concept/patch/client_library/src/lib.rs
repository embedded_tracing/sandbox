use num_cpus::{get, get_physical};
use time::get_current_time;

pub struct Span;

impl Span {
    pub fn new() -> Self {
        println!("Current time: {}", get_current_time().as_u64());
        println!("Num CPUs: {}", get());
        Span
    }
}

impl Drop for Span {
    fn drop(&mut self) {
        println!("Num physical CPUs: {}", get_physical());
        println!("Current time: {}", get_current_time().as_u64());
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[test]
    fn test_span() {
        let _span = Span::new();
        assert!(true);
    }
}
